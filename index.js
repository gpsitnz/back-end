const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;

app.use(bodyParser.json());

app.post('/create', function (req, res) {
  res.send(req.body);
});

app.listen(port, () => console.log(`API listening on port ${port}`));